import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;
import java.util.Scanner;

public class EncriptacioSAR {
    private static SecretKeySpec secretKey;
    //EXEMPLE MAIN PER AJUDAR EN LA IMPLEMENTACIÓ
    public static void main(String[] args) {
    try {
      System.out.print("Introdueix la contrasenya: ");
      Scanner user_input = new Scanner( System.in);
      String message = user_input. next();
      String hash = byteArrayToHexString(EncriptacioSAR.computeHash(Objects.requireNonNull(encrypt(message, secretKey))));
      System.out.println("El hash (hex string) : " + hash);
    }
    catch (Exception e){
      e.printStackTrace();
    }
  }
    /**
     * Agafa els Bytes de la clau en format String introduida per parametre i la assigna a la variable global secretKey
     *
     * @param  myKey  Variable que s'utilitza en el metode de encriptar asignant el valor de la variable global secreyKey
     */
    public static void setKey(String myKey)
    {
        MessageDigest sha = null;
        try {
            byte[] key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    /**
     * Fa hash a una String rebuda per parametre i retorna una array de bytes
     *
     * @param  x  El text al que li farem el hash
     */
  public static byte[] computeHash(String x) throws Exception
  {
     java.security.MessageDigest d =null;
     d = java.security.MessageDigest.getInstance("SHA-1");
     d.reset();
     d.update(x.getBytes());
     return  d.digest();
  }
    /**
     * Transforma una Array de bytes a una String hexadecimal
     *
     * @param  b  la array a transformar
     */
  public static String byteArrayToHexString(byte[] b){
     StringBuffer sb = new StringBuffer(b.length * 2);
     for (int i = 0; i < b.length; i++){
       int v = b[i] & 0xff;
       if (v < 16) {
         sb.append('0');
       }
       sb.append(Integer.toHexString(v));
     }
     return sb.toString().toUpperCase();
  }
    /**
     * Encripta la variable String amb el algoritme AES i el retorna codificat en Base64
     *
     * @param  strToEncrypt  El text a encriptar
     * @param  secret La clau generada amb el metode setKey
     */
    public static String encrypt(String strToEncrypt, SecretKeySpec secret)
    {
        try
        {
            setKey(String.valueOf(secret));
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        }
        catch (Exception e)
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
}